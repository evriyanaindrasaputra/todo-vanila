// create variable to get element on index.html
const formTodo = document.querySelector("#form-todo");
const inputTodo = document.querySelector("#input-todo");
const buttonSubmit = document.querySelector("#submit-todo");
const todoList = document.querySelector("#todo-list");

let todos = [];

// handle action when click button submit
buttonSubmit.addEventListener("click", function (e) {
  e.preventDefault();
  // read value from input
  const todo = inputTodo.value;
  // check if input is empty
  if (todo.trim() === "") {
    alert("Please fill the form");
  }
  // if input is not empty
  else {
    // add todo to array
    const todoItem = {
      id: todos.length + 1,
      content: todo,
      isDone: false,
    };
    todos.push(todoItem);
    // render todo to list
    renderTodo();
    // clear input
    inputTodo.value = "";
  }
});

// handle action when click on list todo
todoList.addEventListener("click", function (e) {
  // get element that user click
  const element = e.target;
  // check if element is button
  if (element.tagName === "BUTTON" && element.innerText === "Remove") {
    // get id of todo that user click
    const id = element.parentElement.getAttribute("id");
    // find index of todo that user click
    const index = todos.findIndex((todo) => todo.id === parseInt(id));
    // remove todo from array
    todos.splice(index, 1);
    // render todo to list
    renderTodo();
  } else if (element.tagName === "BUTTON" && element.innerText === "Done") {
    // get id of todo that user click
    const id = element.parentElement.getAttribute("id");
    // find index of todo that user click
    const index = todos.findIndex((todo) => todo.id === parseInt(id));
    // change isDone of todo
    todos[index].isDone = true;
    // render todo to list
    renderTodo();
  } else if (element.tagName === "BUTTON" && element.innerText === "Not Done") {
    // get id of todo that user click
    const id = element.parentElement.getAttribute("id");
    // find index of todo that user click
    const index = todos.findIndex((todo) => todo.id === parseInt(id));
    // change isDone of todo
    todos[index].isDone = false;
    // render todo to list
    renderTodo();
  }
});

// function renderTodo
function renderTodo() {
  // get element todo-list
  // clear todo-list
  todoList.innerHTML = "";
  // looping todos
  for (let i = 0; i < todos.length; i++) {
    // create elemt li
    const li = document.createElement("li");
    // add id to li
    li.setAttribute("id", todos[i].id);
    // add class to li
    li.classList.add("flex", "items-center", "py-2");
    // create element p
    const p = document.createElement("p");
    // add class to p
    p.classList.add("w-full", "text-gray-400", "flex-1");
    // add text to p
    p.textContent = todos[i].content;
    // add p to li
    li.appendChild(p);
    if (todos[i].isDone === false) {
      // create element buttonDone
      const buttonDone = document.createElement("button");
      // add class to buttonDone
      buttonDone.classList.add(
        "flex-no-shrink",
        "p-2",
        "ml-4",
        "mr-2",
        "border-2",
        "rounded",
        "hover:text-white",
        "text-green-200",
        "border-green-200",
        "hover:bg-green-400"
      );
      // add text to buttonDone
      buttonDone.innerText = "Done";
      // add buttonDone to li
      li.appendChild(buttonDone);
    } else if (todos[i].isDone === true) {
      // create element buttonNotDone
      const buttonNotDone = document.createElement("button");
      // add class to buttonNotDone
      buttonNotDone.classList.add(
        "flex-no-shrink",
        "p-2",
        "ml-4",
        "mr-2",
        "border-2",
        "rounded",
        "hover:text-white",
        "text-gray-200",
        "border-gray-200",
        "hover:bg-orange-400"
      );
      // add text to buttonNotDone
      buttonNotDone.innerText = "Not Done";
      // add buttonNotDone to li
      li.appendChild(buttonNotDone);
    }
    // create element buttonRemove
    const buttonRemove = document.createElement("button");
    // add class to buttonRemove
    buttonRemove.classList.add(
      "flex-no-shrink",
      "p-2",
      "ml-2",
      "border-2",
      "rounded",
      "text-red-200",
      "border-red-200",
      "hover:text-white",
      "hover:bg-red-400"
    );
    // add text to buttonRemove
    buttonRemove.innerText = "Remove";
    // add buttonRemove to li
    li.appendChild(buttonRemove);
    todoList.appendChild(li);
  }
}
